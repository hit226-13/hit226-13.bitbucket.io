
var cartList = [];
var subtotal = 0;

var total = 0;

var shippingSelected = 0;


var discountToggle = 0;

const itemList = [{

    id: 0,

    name: "BMAX X15 Laptop 15.6 inch" ,

    description: "The new Bmax X15 is equipped with aluminum alloy material," +
                "which can protect your laptop and look more gorgeous than ever.",

    img: "https://i.postimg.cc/zHvJCyXJ/1.jpg",

    price: 439.48,

    stock: "in",

    rating: 1

},
{
    id: 1,

    name: "Teclast F7 Plus Ⅲ Laptop 14.1 inch",

    description: "Powered by the new Intel Gemini Lake N4120 Quad-Core processor,"+
                "offers you smooth operation and a stable multitasking experience.",

    img: "https://i.postimg.cc/xqpj03tq/2.jpg",

    price: 754.20,

    stock: "in",

    rating: 5
},
{
    id: 2,

    name: "T-BAO X8S Laptop 15.6 Inch",

    description: "T-Bao X8S comes with the quad processor  Intel J4125 CPU," +
                "and the frequency features 2.0GHz to 2.7GHz, giving more performances to improve efficiency.",

    img: "https://i.postimg.cc/vDZHFN7p/3.jpg",

    price: 609.60,

    stock: "low",

    rating: 3

},
{
    id: 3,

    name: "HP Shadow Elf 6",

    description: "HP Shadow Elf 6 is pre-installed with Windows 10 Home Basic operating system, a home Gaming Laptop",

    img: "https://i.postimg.cc/BnYCq8rd/lap.jpg",

    price: 990.00,

    stock: "sale",

    rating:4.8


},
{
    id: 4,

    name: "Lenovo ThinkPad E15 01CD",

    description: "Lenovo ThinkPad E15 01CD is a home office,"+
    " light and portable, student computer, audio-visual entertainment, cost-effective laptop",

    img: "https://i.postimg.cc/J4R3Zcmf/lap2.jpg",

    price: 769.00,

    stock: "out",

    rating: 4
},
{
    id: 5,

    name: "DERE MBook M11 Laptop 15.6 Inch",

    description: "The DERE MBook M11 laptop has a 15.6-inch IPS screen with a resolution of 1080p and a bezel of just 5mm,"+
    " with a fingerprint recognition module and Windows 10 ",

    img: "https://i.postimg.cc/dtYB6df0/prod6.jpg",

    price: 664.35,

    stock: "low",

    rating:4.9


}

]
/*
document.getElementById("info-tag").addEventListener("hover", toggle)

function toggleinfoTagContent(){
  document.getElementById ("info-tag-popup").
}
*/

const shippingList = [{
    name: "expressShipping",

    price: 15
},

{
    name: "standardShipping",

    price: 5
},

{
    name: "EconomyShipping",

    price: 2
}]



//Cart Logic

function addItem(i) {
    if (itemList[i].stock === "out") {
        alert("Sorry item is out of stock");

    }
    else if (itemList[i].stock === "sale") {
        alert("Sale!");
        cartList.push(i);

        getsubtotal();

        updateDp();

        shipping();

        updateTotals();

        alert("Added item to cart: " + itemList[i].name);

        discount();
    }
    else {
        cartList.push(i);

        getsubtotal();

        updateDp();

        shipping();

        updateTotals();

        alert("Added item to cart: " + itemList[i].name);

        discount();


    }
    localStorage.setItem("list", JSON.stringify(cartList));
    console.log(cartList);

}


function getsubtotal() {
    subtotal = 0;

    cartList.forEach((id) => {

        subtotal += itemList[id].price;

    });

    var calc = parseInt((subtotal) / 0.01);
    console.log(calc)
    subtotal = calc * 0.01

    discount();
    console.log(subtotal);
    return subtotal;
}

function getTotal() {

    total = getsubtotal() + shipping();
    return total;

}


function shipping() {

    var shippingPrice = 0;

    var count = 0;

    var rbs = document.querySelectorAll("input[name=\"shipBtn\"]");

    rbs.forEach((rb) => {

        if (rb.value == "shippingButton1" & rb.checked) {

            shippingPrice = shippingList[count].price;
            shippingSelected = 1;
            console.log(count);


        }

        if (rb.value == "shippingButton2" & rb.checked) {

            shippingPrice = shippingList[count].price;
            shippingSelected = 1;
            console.log(count);


        }

        if (rb.value == "shippingButton3" & rb.checked) {

            shippingPrice = shippingList[count].price;
            shippingSelected = 1;
            console.log(count);

        }

        count++;


    }

    )


    return shippingPrice;

}

function removeItem(i) {
    //WARNING: Doesn't handle outOfBounds Exception

    cartList.splice(i, 1);

    getsubtotal();

    updateDp();

    updateTotals();

    discount();

    alert("Removed item from cart: " + itemList[i].name);
    localStorage.removeItem(i);



}


function discount() {

    if (subtotal >= 100) {
        if (discountToggle == 0) {
            alert("Congratulations 5% discount when you spend over $100 applied");
            discountToggle = 1;
        }

        var calc = parseInt((0.95 * subtotal) / 0.01);
        console.log(calc)
        subtotal = calc * 0.01

    }

}
//End Cart Logic
//Extra functions

function findId(val) {

    for (var i = 0; i < itemList.length; i++) {

        if (val == itemList[i].name) {

            subtotal += itemList[i].price;

            break;

        }

    }

}


//Display properties
function updateDp() {

    cartStr = "Cart List";

    priceStr = "Price";



    cartList.forEach((id) => {

        cartStr += "<p style=\"display: flex; align-items: baseline; flex-direction: row;  box-sizing: border-box;\">" + itemList[id].name + "</p>";

        priceStr += "<p style=\"display: flex; align-items: baseline; flex-direction: row;  box-sizing: border-box;\">$" + itemList[id].price + "</p>";



    }

    )

    document.getElementById("cartListDp").innerHTML = cartStr;

    document.getElementById("priceListDp").innerHTML = priceStr;



}

function updateTotals() {

    if (shipping() > 0) {

        totalStr = "TOTAL PRICE(inc. GST):" + "<p>$" + getTotal() + "</p>";

        document.getElementById("total").innerHTML = totalStr;

    }

    subtotalStr = "SUBTOTAL PRICE(inc. GST):" + "<p>$" + getsubtotal() + "</p>";

    document.getElementById("subtotalDp").innerHTML = subtotalStr;


}

//Buttons   - Needed to add this because generating items made addItem undefined in HTML.

function addItemBtn(i) {

    return addItem(i);
}

//Creates string for star operating
function starRatingGenerator(i){
    if (((i-(i%0.5))/0.5)%2 == 0){
      return "<span class = \"fa fa-star checked\"></span>".repeat(i-(i%0.5)) + "<span class = \"fa fa-star unchecked\"></span>".repeat(5-(i-(i%0.5)))
    }
    else{
      return "<span class = \"fa fa-star checked\"></span>".repeat(i-(i%0.5)+0.5) + "<span class = \"fa fa-star unchecked\"></span>".repeat(5-(i-(i%0.5)+0.5))
    }

}

//Item generator
function generate() {
    // Load Header
    const header = document.querySelector("header");
    //header.innerHTML = "<p><img alt=\"logo\" src=\"logosmall.jpg\"></p>";

    //Load navbar

    //Load Body Content

    var generateStr = "";

    for (var item = 0; item < itemList.length; item++) {



        generateStr += "<div class=\"card stock " + itemList[item].stock+"\">"  +
        " <div class=\"info-section\" <span class=\"info-tag-"+item+"\" style = \"" +
          " z-index:5;   font-size: 18px;\" onmouseover = \"document.getElementById('info-tag-popup " +item+"').style.visibility='visible';\""+
          " onmouseout  = \"document.getElementById('info-tag-popup " +item+"').style.visibility='hidden';\"> &#x1F6C8</span>"+
          " <span class=\"info-tag-popup "+item+"\" id=\"info-tag-popup "+item+"\" >"
          + "<div class=\"popup-description\"><h5 style=\"margin-bottom=2px;\"><b>Description</b></h5>"+
                    itemList[item].description+"</div><div class=\"popup-star-rating\">" +starRatingGenerator(itemList[item].rating)+"</div>"+
                    "</span></div>" +
              "  <div class=\"card-image outer\">" +
            "<div class=\"card-image inner\" id=\"image\">" +
            "<img src=" + itemList[item].img + ">" +
            "<alt=\"Orange\" />" +
            "</div>" +
            "</div>" +
            "<div>" +
            "<div class=\"card-body\" id=\"card_body\">" +
            "<div class=\"card-title1\">" +
            "<h6>" +

            "</h6>" +
            "</div>" +
            "<div class=\"card-title2\">" +
            "<h3>" +
            itemList[item].name +
            "</h3>" +
            "</div>" +
            "<div class=\"card-content\">" +
            "<button id=\"add\" onclick=\"addItemBtn(" + item + ")\"> Add to cart </button>" +
            "<p>" +
            "Price: $" + itemList[item].price +
            "</p>" +
            "</div>" +
            "</div>" +
            "</div>"
            + "</div>"
            + "</div>"

    }


    document.getElementById("itemContainer").innerHTML = generateStr;
}


function cart() {

    const header = document.querySelector("header");
    header.innerHTML = "<p><img src=\"logosmall.jpg\"></p>";


    var item = JSON.parse(localStorage.getItem("list"));
    console.log(item);
    var generateStr = "";
    for (var i = 0; i < item.length; i++) {
        var citem = item[i]
        console.log(citem);
        generateStr += "<div class=\"card\">"  +
            "<div id=\"image2\" onclick=\"testDisplay()\">"  +
              "  <div class=\"card-image outer\">" +
                  " <div class=\"info-section\"><span class=\"info-tag-"+citem+"\" style = \"" +
                    "    font-size: 18px;\" onmouseover = \"document.getElementById('info-tag-popup-" +citem+"').style.visibility='visible';\""+
                    " onmouseout  = \"document.getElementById('info-tag-popup-" +citem+"').style.visibility='hidden';\"> &#x1F6C8</span>"+
                    " <span class=\"info-tag-popup-"+citem+"\" id=\"info-tag-popup-"+citem+"\" style= \"" +
                      "  visibility: hidden;" +
                      "  width: 160px;" +
                      "  height: 160px;" +
                      "  margin-left: 210px;" +
                      "  margin-bottom: 200px;" +
                      "  background-color: #555;" +
                      "  color: #fff;"  +
                      "  text-align: center;" +
                      "  border-radius: 6px;" +
                      "  padding: 8px 0;" +
                      "  position: fixed;" +
                      "  z-index: 5;" +
                      "  left: 50%;" +
                      "  overflow: visible;\"" +

                  " >"+itemList[citem].description+"</span></div>" +
            "<div class=\"card-image inner\" id=\"image\">" +
            "<img src=" + itemList[citem].img +
            "alt=\"laptop\" />" +
            "</div>" +
            "</div>" +
            "<div>" +
            "<div class=\"card-body\" id=\"card_body\">" +
            "<div class=\"card-title1\">" +
            "<h6>" +

            "</h6>" +
            "</div>" +
            "<div class=\"card-title2\">" +
            "<h3>" +
            itemList[citem].name +
            "</h3>" +
            "</div>" +
            "<div class=\"card-content\">" +
            "<button id=\"add\" onclick=\"addItemBtn(" + item + ")\"> Add  </button>" +
            "<button id=\"delete\" onclick=\"removeItem(" + item + ")\"> Delete </button>" +
            "<p>" +
            "Price: $" + itemList[citem].price +
            "</p>" +
            "</div>" +
            "</div>" +
            "<button id=\"Moreinfo\" onclick=\"location.href='https://www.mcwareitsolutions.com/faq'\"> ? </button>" +
            "</div>"+
             "</div>"
            + "</div>"
        addItemBtn(citem)
    }



    document.getElementById("itemContainer").innerHTML = generateStr;

}
function detail() {
    const header = document.querySelector("header");
    header.innerHTML = "<p><img src=\"logosmall.jpg\"></p>";
    var generateStr = "";
    generateStr += "<div class=\"outer\">" +
        "<div class=\"middle\">" +
        "<div class=\"inner\">" +
        "<img src=" + "https://i.postimg.cc/pd6CJSNN/prod1.jpg" + ">" +
        "</div>" +
        "</div>" +
        "</div>"+
        "<div class=\"description\">" +
        "<h1>descriptiondescriptiondes</h1>" +
        "</div>"
    document.getElementById("itemContainer").innerHTML = generateStr;

}
