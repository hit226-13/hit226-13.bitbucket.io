const itemList = [{

    id: 0,

    name: "BMAX X15 Laptop 15.6 inch" ,

    description: "The new Bmax X15 is equipped with aluminum alloy material," +
                "which can protect your laptop and look more gorgeous than ever.",

    img: "https://i.postimg.cc/zHvJCyXJ/1.webp",

    price: 439.48,

    stock: "in",

    rating: 4.84

},
{
    id: 1,

    name: "Teclast F7 Plus Ⅲ Laptop 14.1 inch",

    description: "Powered by the new Intel Gemini Lake N4120 Quad-Core processor,"+
                "offers you smooth operation and a stable multitasking experience.",

    img: "https://i.postimg.cc/xqpj03tq/2.webp",

    price: 754.20,

    stock: "in",

    rating: 5
},
{
    id: 2,

    name: "T-BAO X8S Laptop 15.6 Inch",

    description: "T-Bao X8S comes with the quad processor  Intel J4125 CPU," +
                "and the frequency features 2.0GHz to 2.7GHz, giving more performances to improve efficiency.",

    img: "https://i.postimg.cc/vDZHFN7p/3.webp",

    price: 609.60,

    stock: "low",

    rating: 3

},
{
    id: 3,

    name: "HP Shadow Elf 6",

    description: "HP Shadow Elf 6 is pre-installed with Windows 10 Home Basic operating system, a home Gaming Laptop",

    img: "https://i.postimg.cc/BnYCq8rd/lap.jpg",

    price: 990.00,

    stock: "sale",

    rating:4.8


},
{
    id: 4,

    name: "Lenovo ThinkPad E15 01CD",

    description: "Lenovo ThinkPad E15 01CD is a home office,"+
    " light and portable, student computer, audio-visual entertainment, cost-effective laptop",

    img: "https://i.postimg.cc/J4R3Zcmf/lap2.jpg",

    price: 769.00,

    stock: "out",

    rating:4.7
},
{
    id: 5,

    name: "DERE MBook M11 Laptop 15.6 Inch",

    description: "The DERE MBook M11 laptop has a 15.6-inch IPS screen with a resolution of 1080p and a bezel of just 5mm,"+
    " with a fingerprint recognition module and Windows 10 ",

    img: "https://i.postimg.cc/dtYB6df0/prod6.jpg",

    price: 664.35,

    stock: "low",

    rating:4.9


},
{
    id: 6,

    name: "BlitzWolf bluetooth Earphone",

    description: "",

    img: "https://i.postimg.cc/8Pmb0c8m/prod7.jpg",

    price: 67.53,

    stock: "out"
},
{
    id: 7,

    name: "WLtoys 4WD Crawler RC Car",

    description: "",

    img: "https://i.postimg.cc/P5fMsHTs/prod8.jpg",

    price: 121.30,

    stock: "in"
},
{
    id: 8,

    name: "Eachine Tyro109 210mm Racing Drone",

    description: "",

    img: "https://i.postimg.cc/mgdyTngc/prod9.jpg",

    price: 222.45,

    stock: "out"
},
{
    id: 9,

    name: "URUAV 12cm XT60 Male Plug Adapter Cable",

    description: "",

    img: "https://i.postimg.cc/d0Lm39h5/prod10.jpg",

    price: 9.91,

    stock: "sale"

},
{
    id: 10,

    name: "Retevis Walkie Talkie",

    description: "",

    img: "https://i.postimg.cc/zf6wgRW8/prod11.jpg",

    price: 66.42,

    stock: "out"
},
{
    id: 11,

    name: "Flashhobby 2600kv 3-4s Brushless Motor",

    description: "",

    img: "https://i.postimg.cc/LXPtB6Bf/prod12.jpg",

    price: 37.54,

    stock: "in"
}
]

window.itemList = itemList;
/*
document.getElementById("info-tag").addEventListener("hover", toggle)

function toggleinfoTagContent(){
  document.getElementById ("info-tag-popup").
}
*/
