



var cartList=[];

var subtotal = 0;

var total=0;

var shippingSelected=0;

var discountToggle=0;
 
const itemList = [{

        id: 0,

        name: "Fast Charging Wireless Adapter",

        img: "https://i.postimg.cc/pd6CJSNN/prod1.jpg",

        price:22.23,
		
		stock: "in"

},
{
        id:1,

        name: "Gosund Smart Light Bulbs",

        img: "https://i.postimg.cc/Kz3Nq5F1/prod2.jpg",

        price:24.54,
		
		stock: "in"
},
{      
        id: 2,

        name: "15.6 inches Laptop Backpack Waterproof",

        img: "https://i.postimg.cc/kGryktXf/prod3.jpg",

        price:72.20,
		
		stock: "low"

},
{
        id:3,

        name: "1080p Pan/Tilt Wifi Camera",

        img: "https://i.postimg.cc/VvjF1LJ1/prod4.jpg",

        price:50.33,
		
		stock: "sale"


},
{
        id:4,

        name:"Senbono s11 Smart Watch",

        img: "https://i.postimg.cc/ryWj5gQb/prod5.jpg",

        price:28.87,
		
		stock: "out"
},
{
        id:5,

        name:"DERE MBook M11 Laptop 15.6 Inch",

        img:"https://i.postimg.cc/dtYB6df0/prod6.jpg",

        price:664.35,
		
		stock: "low"
		
		
},
{
        id:6,

        name:"BlitzWolf bluetooth Earphone",
        
        img:"https://i.postimg.cc/8Pmb0c8m/prod7.jpg",

        price:67.53,
		
		stock: "out"
},
{
        id:7,

        name:"WLtoys 4WD Crawler RC Car",

        img:"https://i.postimg.cc/P5fMsHTs/prod8.jpg",

        price:121.30,
		
		stock: "in"
},
{
        id:8,

        name:"Eachine Tyro109 210mm Racing Drone",

        img:"https://i.postimg.cc/mgdyTngc/prod9.jpg",

        price:222.45,
		
		stock: "out"
},
{
        id:9,

        name:"URUAV 12cm XT60 Male Plug Adapter Cable",

        img:"https://i.postimg.cc/d0Lm39h5/prod10.jpg",

        price:9.91,
		
		stock: "sale"

},
{
        id:10,

        name:"Retevis Walkie Talkie",

        img:"https://i.postimg.cc/zf6wgRW8/prod11.jpg",

        price:66.42,
		
		stock: "out"
},
{
        id:11,

        name:"Flashhobby 2600kv 3-4s Brushless Motor",

        img:"https://i.postimg.cc/LXPtB6Bf/prod12.jpg",

        price:37.54,
		
		stock: "in"
}
]

const shippingList = [{name: "expressShipping",
                       
                       price: 15},
                       
                       {name: "standardShipping",
                       
                       price:5},

                       {name: "EconomyShipping",

                       price: 2}]


//Cart Logic

function addItem(i){
if (itemList[i].stock === "out" ){
	alert("Sorry item is out of stock"); 
    
}
else if(itemList[i].stock === "sale"){
	alert("Sale!");
cartList.push(i);
    
    getsubtotal();

    updateDp();

    shipping();

    updateTotals();    
    
    alert("Added item to cart: "+itemList[i].name); 
    
    discount();	
}
else{
	cartList.push(i);
    
    getsubtotal();

    updateDp();

    shipping();

    updateTotals();    
    
    alert("Added item to cart: "+itemList[i].name); 
    
    discount();
	
	
}

}	
    

function getsubtotal(){
    subtotal = 0;

    cartList.forEach((id) => {

        subtotal+=itemList[id].price;

    });

	var calc = parseInt((subtotal)/0.01);
		console.log(calc)
        subtotal = calc*0.01

    discount();

    return subtotal;
}

function getTotal(){
    
    total = getsubtotal()+shipping();
    return total;

}


function shipping(){

    var shippingPrice=0;

    var count=0;

    var rbs = document.querySelectorAll("input[name=\"shipBtn\"]");
    
    rbs.forEach((rb) =>{
        
        if (rb.value == "shippingButton1" & rb.checked){
       
            shippingPrice = shippingList[count].price;
            shippingSelected=1;
            console.log(count);
            
       
        }     

        if (rb.value == "shippingButton2" & rb.checked){
       
            shippingPrice = shippingList[count].price;
            shippingSelected=1;
            console.log(count);
            
       
        }  
        
        if (rb.value == "shippingButton3" & rb.checked){
       
            shippingPrice = shippingList[count].price;
            shippingSelected=1;
            console.log(count);
            
        }  

        count++;

        
    }
    
    )
    
        
    return shippingPrice;

}

function removeItem(i){
    //WARNING: Doesn't handle outOfBounds Exception
    
    cartList.splice(i, 1);
    
    getsubtotal();
    
    updateDp();

    updateTotals();   

    discount();

    alert("Removed item from cart: "+itemList[i].name); 

    
}


function discount(){

    if (subtotal>=100){
        if (discountToggle==0){
            alert("Congratulations 5% discount when you spend over $100 applied");
            discountToggle=1;
        }

		var calc = parseInt((0.95*subtotal)/0.01);
		console.log(calc)
        subtotal = calc*0.01

    }

}
//End Cart Logic
//Extra functions

function findId(val){
    
    for (var i = 0; i<itemList.length;i++){

        if (val == itemList[i].name){

            subtotal+=itemList[i].price;

            break;
        
        }
    
    }
    
}


//Display properties
function updateDp(){

    cartStr="Cart List";

    priceStr="Price";

   

    cartList.forEach((id)=>{
    
        cartStr += "<p style=\"display: flex; align-items: baseline; flex-direction: row;  box-sizing: border-box;\">"+ itemList[id].name+"</p>";

        priceStr += "<p style=\"display: flex; align-items: baseline; flex-direction: row;  box-sizing: border-box;\">$"+ itemList[id].price+"</p>";

        
    
    } 

    )
    
    document.getElementById("cartListDp").innerHTML = cartStr;

    document.getElementById("priceListDp").innerHTML=priceStr;

    

}

function updateTotals(){
    
    if (shipping()>0){
        
        totalStr="TOTAL PRICE(inc. GST):" + "<p>$"+getTotal()+"</p>";

        document.getElementById("total").innerHTML = totalStr;
        
    }

    subtotalStr="SUBTOTAL PRICE(inc. GST):"+ "<p>$"+ getsubtotal()+"</p>";

    document.getElementById("subtotalDp").innerHTML=subtotalStr;

    
}

//Buttons   - Needed to add this because generating items made addItem undefined in HTML.

function addItemBtn(i){

    return addItem(i);
}

//Item generator
function generate() {
    // Load Header
    const header = document.querySelector("header");
    header.innerHTML = "<p><img alt=\"logo\" src=\"logosmall.jpg\"></p>";

    //Load navbar
    const navbar = document.getElementById("navbar");
    navbar.innerHTML = "<a href=\"\">Home</a> <a href=\"\">Category</a><a href=\"\">Log in</a><a href=\"#order\">Cart</a>";

    //Load Body Content   

    var generateStr = "";

    for (var item = 0; item < itemList.length; item++) {
       

        generateStr += "<div class=\"stock " + itemList[item].stock + "\"; style =\"width:300px;padding:10px; margin-bottom:10px;\"><div class=\"innerdiv mx-auto\" style=\"height:180px;width:150px;\"><img alt=" + itemList[item].name+"\" src=" + itemList[item].img + "><p name=" + itemList[item].name + ">" + itemList[item].name +
            "</p></div><div class= \"mx-auto\" style=\" padding-left:65px; \"><p>Price: $" + itemList[item].price + "</p></div><div class= \"mx-auto\" style=\" padding-left:65px; \">" +
            "<button name=\"Add Item\" onclick=\"addItemBtn(" + item + ")\"> Add to cart </button></div></div>"
    }


    //console.log(generateStr);
   
document.getElementById("itemContainer").innerHTML = generateStr;
}
function inherit(){
        const header = document.querySelector("header");
        header.innerHTML = "<p><img src=\"logosmall.jpg\"></p>";
    
        const navbar = document.getElementById("navbar");
        navbar.innerHTML = "<a href=\"\">Home</a> <a href=\"\">Category</a><a href=\"\">Log in</a><a href=\"#order\">Cart</a>";
}

        


    

   











